﻿using Business;
using Global.Platform;
using NUnit.Framework;
using System;

namespace GlobalUnitTests
{
    [TestFixture]
    public class BusinessTheoBLTests
    {
        private TheoBL _theoBL;
        [TestFixtureSetUp]
        public void Init() {

            _theoBL = new TheoBL();
        
        }

        [TestFixtureTearDown]
        public void Dispose() { 
        
        }

        [SetUp]
        public void Setup() { 
        }

        [TearDown]
        public void Teardown()
        {
            
        }

        [Test]
        public void TheoBLShouldGetValidPlatformDefinition()
        {
            PlatformKey pk = new PlatformKey("QA", "ATT", "ICARE", "UI", "PODA");
            var sut = _theoBL.GetPlatformDefinition(pk);
            Assert.That(sut.IsValid());
        }

        [Test]
        public void PlatformDefinitionShouldThrowError() {
            PlatformKey pk = new PlatformKey("QA1", "ATT", "ICARE", "UI", "PODA");
            Assert.Throws<Exception>(() =>
            {
                var sut = _theoBL.GetPlatformDefinition(pk);
            });
        }
    }
}