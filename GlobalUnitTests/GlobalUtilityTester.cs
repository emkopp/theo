﻿using Global.Platform;
using NUnit.Framework;

namespace GlobalUnitTests
{
    [TestFixture]
    public class GLobalUtilityTester
    {
        [Test]
        public void PlatformDefinitionsShouldMatchWithSameValues()
        {
            PlatformDefinition pdf = new PlatformDefinition();
            pdf.AddData("1", "test");
            pdf.AddData("2", "t");
            pdf.AddData("3", "test");
            pdf.AddData("4", "t");
            pdf.AddData("5", "test");
            pdf.AddData("6", "t");
            pdf.AddData("7", "test");
            pdf.AddData("8", "t");

            PlatformDefinition pdf2 = new PlatformDefinition();
            pdf2.AddData("3", "test");
            pdf2.AddData("4", "t");
            pdf2.AddData("5", "test");
            pdf2.AddData("6", "t");
            pdf2.AddData("2", "t");
            pdf2.AddData("1", "test");
            pdf2.AddData("7", "test");
            pdf2.AddData("8", "t");

            Assert.That(pdf.GetStringValueHash() == pdf2.GetStringValueHash());
        }
    }
}