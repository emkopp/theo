﻿using EventRouter;
using Global.DTO;
using Global.Extensions;
using Global.Platform;
using System;
using System.Configuration;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace SignalRClient
{
    internal class Program
    {
        private static HubCommunicator _ahc;
        private static HubCommunicator _thc;

        private static void Main(string[] args)
        {
            var url = ConfigurationManager.AppSettings.Get("hub:url");
            _thc = new HubCommunicator("TheoHub", url, null);
            var thc2 = new HubCommunicator("TheoHub", url, null);
            PlatformKey pk = new PlatformKey("QA", "ATT", "iCare", "UI", "PODA");
            var pdf = _thc.GetPlatformDefinition(pk);

            // SERVERNAME
            try
            {
                _thc.GroupEnrollment(System.Environment.MachineName);
                _thc.GroupEnrollment(pk.GetKey());
                _thc.GroupEnrollment("ICARE-ATT");
                _thc.GroupEnrollment("OTTO-CONNECTOR");
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            
            }

            var action2 = new Action<String>(s =>
            {
                Console.WriteLine(s);
            });

            var pingAction = new Action<CommandDTO>((cdto) =>
            {
                StringBuilder sb = new StringBuilder("Ping Response");
                try
                {
                    var dnsname = cdto.Message;
                    Ping pingSender = new Ping();
                    PingOptions options = new PingOptions();
                    options.DontFragment = true;
                    string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    byte[] buffer = Encoding.ASCII.GetBytes(data);
                    int timeout = 120;
                    PingReply reply = pingSender.Send(dnsname, timeout, buffer, options);
                    var ips = Dns.GetHostAddresses(dnsname);
                    sb.AppendLine(String.Empty);
                    sb.AppendLine(String.Format("Command Group Issued: {0}", cdto.CommandGroup));
                    sb.AppendLine(String.Format("FromServer: {0}", System.Environment.MachineName));
                    sb.AppendLine(String.Format("Destination: {0}", cdto.Message));
                    sb.AppendLine(String.Format("Status: {0}", reply.Status));
                    if (reply.Status == IPStatus.Success)
                    {
                        sb.AppendLine(string.Format("Address: {0}", reply.Address.ToString()));
                        foreach (var ip in ips)
                        {
                            sb.AppendLine(string.Format("IPAddress: {0}", ip.MapToIPv4().ToString()));
                        }

                        sb.AppendLine(string.Format("RoundTrip time: {0}", reply.RoundtripTime));
                        sb.AppendLine(string.Format("Buffer size: {0}", reply.Buffer.Length));
                    }
                    else
                    {
                        sb.AppendLine("There was a failure in Ping reply.");
                    }
                }
                catch (Exception ex) {
                    sb.AppendLine(ex.Message);
                }
                
                Console.WriteLine(sb);
                thc2.TextToRoom(cdto.CallbackGroup, sb.ToString());
            });
            
            var actionPDF = new Action<String, PlatformDefinition>((s, p) =>
            {
                Console.WriteLine(s);
                pdf = p;
            });

            _thc.listener.RegisterListener<CommandDTO>("PingGroup", pingAction);
            _thc.listener.RegisterListener<String>("GroupFeed", action2);
            _thc.listener.RegisterListener<String, PlatformDefinition>("TheoTalkback", actionPDF);

            string cont = String.Empty;
            Boolean isHashGood;
            Console.WriteLine("type --> stop <-- to end loop.");

            while (cont != "stop")
            {
                if (cont == "pdfload")
                {
                    var newpdf = _thc.GetPlatformDefinition(pk);
                    if (pdf.GetStringValueHash() != newpdf.GetStringValueHash())
                    {
                        Console.WriteLine("Client side verification variance found");
                        pdf = newpdf;
                        WritePDFData(pdf);
                    }
                    else
                    {
                        Console.WriteLine("No client variance found");
                    }
                }
                else if (cont == "pdf")
                {
                    WritePDFData(pdf);
                }
                else if (cont == "reload")
                {
                    _thc.ReloadPDF(pk);
                    Console.WriteLine("Platform Definition command to have hub check if PDF is different.");
                }
                else
                {
                    isHashGood = _thc.CheckHash(pk, pdf.GetStringValueHash());
                    Console.WriteLine("Hash is: " + isHashGood.ToString());
                }

                cont = Console.ReadLine();
            }

            Console.WriteLine("Press any key to end application.");
            Console.Read();
        }

        private static void WritePDFData(PlatformDefinition pdf)
        {
            Console.WriteLine("***PDF DETAIL***");
            Console.WriteLine("Key: " + pdf.GetKey());
            Console.WriteLine("Hash: " + pdf.GetStringValueHash());
            Console.WriteLine("KeyCount: " + pdf._dict.Count);
            Console.WriteLine("TimeStamp: " + pdf.MaxTimeStamp);

            Console.WriteLine("*Keys*");
            foreach (var l in pdf._dict)
            {
                Console.WriteLine(String.Format("Key: {0} | Value: {1} ", l.Key, l.Value));
            }
            Console.WriteLine("***END DETAIL***");
        }
    }
}