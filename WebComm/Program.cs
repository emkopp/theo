﻿using EventRouter;
using System;

namespace WebComm
{
    internal class Program
    {
        private static HubCommunicator _ahc;
        private static HubCommunicator _thc;

        private static void Main(string[] args)
        {
            var url = "http://localhost/WebHub";
            _thc = new HubCommunicator("TheoHub", url, null);
            var hello = _thc.Hello();

            Console.WriteLine(hello);

            Console.ReadLine();
        }
    }
}