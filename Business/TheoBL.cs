﻿using Global.Extensions;
using Global.Interfaces;
using Global.Platform;
using Infrastructure.Data;
using Infrastructure.Data.EntityFramework;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace Business
{
    public class TheoBL
    {
        private static ConcurrentDictionary<String, PlatformDefinition> _sets = new ConcurrentDictionary<String, PlatformDefinition>();

        public TheoBL()
        {
        }

        public void CheckKey(IPlatform pk)
        {
            var dbpdf = GetDBPlatformDefinition(pk);
            var ch = IsHashSameAsStore(pk, dbpdf.GetStringValueHash());

            if (!ch)
            {
                RegisterPDF(dbpdf);
            }
        }

        public String GetCurrentHash(IPlatform pk)
        {
            var pdf = GetPlatformDefinition(pk);
            return pdf.GetStringValueHash();
        }

        public PlatformDefinition GetPlatformDefinition(IPlatform pk)
        {
            var pdf = new PlatformDefinition(pk);
            var isSuccess = _sets.TryGetValue(pk.GetKey(), out pdf);

            if (!isSuccess)
            {
                pdf = GetDBPlatformDefinition(pk);

                if (pdf.IsValid())
                {
                    RegisterPDF(pdf);
                }
                else
                {
                    throw new Exception("KeyNotFound");
                }
            }
            return pdf;
        }

        public Boolean IsHashSameAsStore(IPlatform pk, String hash)
        {
            var pdf = GetPlatformDefinition(pk);
            return pdf.GetStringValueHash() == hash;
        }

        public Boolean IsPlatformDefinitionVsStoreSame(PlatformDefinition pdf)
        {
            var storePdf = _sets.GetOrAdd(pdf.GetKey(), pdf);
            return storePdf.GetStringValueHash() == pdf.GetStringValueHash();
        }

        public Boolean IsTimestampSameAsStore(IPlatform pk, byte[] timestamp)
        {
            var pdf = GetPlatformDefinition(pk);
            return pdf.MaxTimeStamp == timestamp;
        }

        public PlatformDefinition ReloadPDF(PlatformKey pk, out Boolean isTouched)
        {
            var dbPdf = GetDBPlatformDefinition(pk);
            isTouched = false;
            var inmemoryPdf = GetPlatformDefinition(pk);
            if (!dbPdf.MaxTimeStamp.SequenceEqual(inmemoryPdf.MaxTimeStamp))
            {
                RegisterPDF(dbPdf);
                isTouched = true;
            }
            var returnPDF = GetPlatformDefinition(pk);
            return returnPDF;
        }

        private PlatformDefinition GetDBPlatformDefinition(IPlatform pk)
        {
            var pdf = new PlatformDefinition(pk);
            try
            {
                using (IRepository<XREFDummy> xd = RepositoryFactory.GetRepository<XREFDummy>())
                {
                    var results = xd.GetAll().Where(w => w.Environment == pk.Environment
                        && w.Client == pk.Client
                        && w.Channel == pk.Channel
                        && w.Layer == pk.Layer
                        && w.Module == pk.Module);

                    pdf.MaxTimeStamp = results.Max(m => m.Timestamp);

                    var dictresults = results.ToDictionary(k => k.Identifier, v => v.Payload);

                    pdf.AddByDictionary(dictresults);
                    return pdf;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void UpdatePDF(IPlatform pk, string key, string value) { 
        
        var pdf = new PlatformDefinition(pk);
            try
            {
                using (IRepository<XREFDummy> xd = RepositoryFactory.GetRepository<XREFDummy>())
                { 
                
                    var x = xd.GetAll().Where( w => w.Channel == pk.Channel 
                                               && w.Client == pk.Client
                                               && w.Environment == pk.Environment
                                               && w.Layer == pk.Layer
                                               && w.Module == pk.Module
                                               && w.Identifier == key
                        ).FirstOrDefault();

                    x.Payload = value;

                    xd.Update(x);
                    xd.Save();

                }
            } catch (Exception e){

                throw e;


            }

        }

        
        



        private void RegisterPDF(PlatformDefinition pdf)
        {
            _sets.AddOrUpdate(pdf.GetKey(), pdf, (key, oldvalue) => pdf);
        }
    }
}