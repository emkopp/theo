﻿using Global.Interfaces;
using System;
using Global.Extensions;

namespace Global.Platform
{
    public class PlatformKey : IPlatform
    {
        public String Environment { get; set; }

        public String Client { get; set; }

        public String Channel { get; set; }

        public String Layer { get; set; }

        public String Module { get; set; }

        public PlatformKey(String environment, String client, String channel, String layer, String module)
        {
            Environment = environment;
            Client = client;
            Channel = channel;
            Layer = layer;
            Module = module;
        }

    }
}