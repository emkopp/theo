﻿using Global.Extensions;
using Global.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Global.Platform
{
    /// <summary>
    /// This is to be serialized and compared to other version of this file via hash to determine change in loaded units
    /// Changes in this file will kick off a series of events to determine if update on client side it needed.
    /// </summary>
    [DataContract]

    public class PlatformDefinition : IPlatform
    {
        [DataMember(Name="Dictionary")]
        public SortedDictionary<string, string> _dict = new SortedDictionary<string, string>();

        public PlatformDefinition()
        {
            //test
        }

        public PlatformDefinition(IPlatform pk)
        {
            Layer = pk.Layer;
            Channel = pk.Channel;
            Environment = pk.Environment;
            Client = pk.Client;
            Module = pk.Module;
        }

        [DataMember]
        public String Channel { get; set; }

        [DataMember]
        public String Client { get; set; }

        [DataMember]
        public String Environment { get; set; }

        //[DataMember]
        //public Dictionary<String, String> KeyStore
        //{
        //    get
        //    {
        //        return _dict.ToDictionary(k => k.Key, k => k.Value);
        //    }
        //}

        [DataMember]
        public String Layer { get; set; }

        [DataMember]
        public byte[] MaxTimeStamp { get; set; }

        [DataMember]
        public String Module { get; set; }

        public void AddByDictionary(Dictionary<String, String> dictionary)
        {
            foreach (var item in dictionary)
            {
                AddData(item.Key, item.Value);
            }
        }

        public void AddData(string key, string value)
        {
            _dict.Add(key, value);
        }

        public byte[] GetByteArrayHash()
        {
            return new byte[] { (byte) 1, (byte) 1};
        }

        public string GetStringValueHash()
        {
            var bytes = GetByteArrayHash();
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

        public Boolean IsValid()
        {
            return _dict.Count > 0
                    && this.IsKeyValid(); ;
        }
    }
}