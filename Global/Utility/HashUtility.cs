﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace Global
{
    public class HashUtility
    {
        public static byte[] HashIt(object o)
        {
            MemoryStream fs = new MemoryStream();
            (new BinaryFormatter()).Serialize(fs, o);
            var objarray = fs.ToArray();
            fs.Close();
            return (new MD5CryptoServiceProvider()).ComputeHash(objarray);
        }
    }
}