﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Global.DTO
{
    public class CommandDTO
    {
        public string Action { get; set; }
        public string CallbackGroup { get; set; }

        public string CommandGroup { get; set; }

        public string Message { get; set; }

        public string FromUser { get; set; }

    }
}
