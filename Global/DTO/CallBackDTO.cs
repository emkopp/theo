﻿using System;

namespace Global.DTO
{
    public class CallBackDTO
    {
        public String Event { get; set; }

        public int PicID { get; set; }
    }
}