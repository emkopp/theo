﻿using System;

namespace Global.DTO
{
    public class CommunicationDTO
    {
        public int SomeNumericValue { get; set; }

        public string Name { get; set; }

        public Byte[] Pic { get; set; }
    }
}