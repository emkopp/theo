﻿using System;

namespace Global.DTO
{
    public class AdminCommunicationDTO
    {
        public string Group { get; set; }

        public String Event { get; set; }

        public String Command { get; set; }

        public String Message { get; set; }
    }
}