﻿using System;
namespace Global.Interfaces
{
    public interface IPlatform
    {
        string Channel { get; set; }
        string Client { get; set; }
        string Environment { get; set; }

        string Layer { get; set; }
        string Module { get; set; }
    }
}
