﻿using System;
using System.Linq;

namespace Global.Interfaces
{
    public interface IRepository<T> : IDisposable
     where T : class, Global.Interfaces.ISurrogate
    {
        IQueryable<T> GetAll();

        T GetById(int id);

        void Update(T entity);

        void Save();

        IQueryable<T> Where(Func<T, bool> predicate);
    }
}