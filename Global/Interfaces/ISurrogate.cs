﻿namespace Global.Interfaces
{
    public interface ISurrogate
    {
        int ID { get; set; }
    }
}