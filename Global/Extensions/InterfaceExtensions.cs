﻿using Global.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global.Extensions
{
    public static class InterfaceExtensions
    {
        public static String GetKey(this IPlatform platform) {
            return String.Format("{0}-{1}-{2}-{3}-{4}", platform.Environment, platform.Client, platform.Channel, platform.Layer, platform.Module);
        }

        public static Boolean IsKeyValid(this IPlatform platform) {

            return !String.IsNullOrWhiteSpace(platform.Environment)
                     || !String.IsNullOrWhiteSpace(platform.Client)
                     || !String.IsNullOrWhiteSpace(platform.Channel)
                     || !String.IsNullOrWhiteSpace(platform.Layer)
                     || !String.IsNullOrWhiteSpace(platform.Module);
        }

    }
}
