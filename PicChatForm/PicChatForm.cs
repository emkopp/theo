﻿using EventRouter;
using System;
using System.Configuration;
using System.Windows.Forms;

namespace PicChatForm
{
    public partial class PicChatForm : Form
    {
        public PicChatForm()
        {
            InitializeComponent();
        }

        private HubCommunicator _adminHub;
        private Listener _listener;
        private Boolean isConnected = false;

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void btn_connect_Click(object sender, EventArgs e)
        {
            var url = ConfigurationManager.AppSettings.Get("hub:url");
            var clientId = tb_clientId.Text;

            _adminHub = new HubCommunicator("AdminHub", url, clientId);
            _listener = _adminHub.listener;
            _adminHub.GroupEnrollment("Test1");
            _adminHub.GroupEnrollment("Test2");

            listBox1.Items.Clear();
            listBox2.Items.Clear();

            foreach (var x in _adminHub.GetAllGroups())
            {
                listBox1.Items.Add(x);
            }

            foreach (var u in _adminHub.GetAllUsers())
            {
                listBox2.Items.Add(u.Key);
            }
        }

        public void SetItemBox3(String s)
        {
            if (InvokeRequired)
            {
                Invoke((Action<string>)SetItemBox3, s);
                return;
            }
            listBox3.Items.Add(s);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var action = new Action<String>(s =>
            {
                listBox3.Items.Add(s);
                listBox3.Refresh();

                //Console.WriteLine(s);
            });

            var action2 = new Action<String>(s =>
            {
                SetItemBox3(s);
            });

            _listener.RegisterListener<String>("TimerCheck", action2);
            _listener.RegisterListener<String>("FormEvents", action);
        }
    }
}