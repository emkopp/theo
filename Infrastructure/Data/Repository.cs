﻿using Global.Interfaces;
using Infrastructure.Data.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Infrastructure.Data
{
    public class Repository<T> : IRepository<T> where T : class , ISurrogate
    {
        private TheOEntities context = new TheOEntities();

        public Repository()
        {
        }
        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }     
        public IQueryable<T> GetAll()
        {
            return context.Set<T>();
        }

        public T GetById(int id)
        {
            return context.Set<T>().Where(w => w.ID == id).FirstOrDefault();
        }

        public IQueryable<T> Where(Func<T, bool> predicate)
        {
            return context.Set<T>().Where(predicate).AsQueryable();
        }

        private void Dispose(bool doDispose)
        {
            if (doDispose)
            {
                context.Dispose();
                GC.SuppressFinalize(this);
            }
        }

        public void Update(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
            {
                context.Set<T>().Attach(entity);
                context.Entry(entity).State = EntityState.Modified;
            }
        }


        public void Dispose()
        {
            Dispose(true);
        }
    }
}