﻿using Global.Interfaces;

namespace Infrastructure.Data
{
    public static class RepositoryFactory
    {
        public static IRepository<T> GetRepository<T>() where T : class, ISurrogate
        {
            return new Repository<T>();
        }
    }
}