﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebHub
{
    public class BaseHub : Hub
    {
        public static Task _t;

        public static ConcurrentDictionary<String, Dictionary<String, String>> HubUsers = new ConcurrentDictionary<String, Dictionary<String, String>>();
        public static ConcurrentDictionary<string, List<Tuple<String, String, String>>> UsersAndGroups = new ConcurrentDictionary<string, List<Tuple<String, String, String>>>();

        static BaseHub()
        {
        }

        public BaseHub()
        {
        }

        public void ExitGroup(string groupName)
        {
            var clientId = GetClientId();
            var message = String.Format("{2} : Group removal done for user {0} to Group {1}", clientId, groupName, this.ToString());
            var hg = GetClassSpecificGroup(this.ToString());

            Groups.Remove(Context.ConnectionId, groupName);

            hg.RemoveAll(r => r.Item1 == clientId && r.Item2 == groupName);
            Console.WriteLine(message);
            Clients.Caller.AdminFeed(message);
        }

        public IEnumerable<String> GetAllGroups()
        {
            var tg = GetClassSpecificGroup(this.ToString());
            return tg.Select(s => s.Item2).Distinct();
        }

        public Dictionary<String, String> GetAllUsers()
        {
            return GetClassSpecificUser();
        }

        public void JoinGroup(string groupName)
        {
            Groups.Add(Context.ConnectionId, groupName);
            AddOrUpdateClassSpecificGroup(this.ToString(), GetClientId(), groupName);
            var message = String.Format("{2} : Group enrollment done for user {0} to Group {1}", GetClientId(), groupName, this.ToString());
            Console.WriteLine(message);
            Clients.Caller.AdminFeed(message);
        }

        public override Task OnConnected()
        {
            String clientId = GetClientId();
            if (!IsHubUsersInitialized()
                    || !IsUserInThisHub(clientId))
            {
                RegisterUser(clientId);
            }
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string clientId = GetClientId();
            if (IsHubUsersInitialized()
                    && (IsUserInThisHub(clientId)))
            {
                RemoveClassSpecificUser(clientId);
                RemoveClassSpecificUserGroups(clientId);
                Console.WriteLine(String.Format("{1} : User Disconnected: {0}", clientId, this.ToString()));
            }
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            String clientId = GetClientId();
            if (!IsHubUsersInitialized()
                    || !(IsUserInThisHub(clientId)))
            {
                RegisterUser(clientId);
            }
            return base.OnConnected();
        }

        protected string GetClientId()
        {
            String clientId = String.Empty;

            if (Context.QueryString["clientId"] != null)
            {
                clientId = this.Context.QueryString["clientId"];
            }
            if (String.IsNullOrEmpty(clientId))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }

        private void AddOrUpdateClassSpecificGroup(String key, String clientId, string groupName)
        {
            var tupleList = UsersAndGroups.Where(w => w.Key == key).Select(s => s.Value).FirstOrDefault();
            if (tupleList == null)
            {
                var lst = new List<Tuple<String, String, String>>();
                lst.Add(new Tuple<String, String, String>(clientId, groupName, ""));

                //UsersAndGroups.GetOrAdd(this.ToString(), lst);
                UsersAndGroups.AddOrUpdate(this.ToString(), lst, (k, v) => v);
            }
            else
            {
                var tupleWorking = tupleList.Where(w => w.Item1 == clientId && w.Item2 == groupName).ToList();
                if (tupleWorking.Count() > 0)
                {
                    //Already in the set
                }
                else
                {
                    try
                    {
                        var lst = GetClassSpecificGroup(this.ToString());
                        lst.Add(new Tuple<String, String, String>(clientId, groupName, ""));

                        //UsersAndGroups.AddOrUpdate(this.ToString(), lst, (k, v) => v);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        private void AddOrUpdateClassSpecificUser(Dictionary<String, String> hsg)
        {
            HubUsers.AddOrUpdate(this.ToString(), hsg, (key, group) => group);
        }

        private List<Tuple<String, String, String>> GetClassSpecificGroup(String key)
        {
            var tupleList = UsersAndGroups.Where(w => w.Key == key).FirstOrDefault();
            return tupleList.Value ?? new List<Tuple<String, String, String>>();
        }

        private Dictionary<String, String> GetClassSpecificUser()
        {
            Dictionary<String, String> hg;
            if (!HubUsers.TryGetValue(this.ToString(), out hg))
            {
                hg = new Dictionary<String, String>();
            }
            return hg;
        }

        private Boolean IsHubUsersInitialized()
        {
            var hubcheck = false;
            try {
                hubcheck = (HubUsers.Count > 0 && HubUsers[this.ToString()].Count > 0);
            }catch(Exception ex){
                Console.WriteLine(ex.Message);
            }
            return hubcheck;
        }

        private Boolean IsUserInThisHub(String clientId)
        {
            var hubcheck = false;
            try {
                hubcheck = (HubUsers[this.ToString()].ContainsKey(clientId));
            }catch(Exception ex){
                Console.WriteLine(ex.Message);
            }
            return hubcheck;
        }

        private void RegisterUser(String clientId)
        {
            var x = GetClassSpecificUser();
            x.Add(clientId, Context.ConnectionId);
            AddOrUpdateClassSpecificUser(x);
            Console.WriteLine(String.Format("{1} : User Reconnected: {0}", clientId, this.ToString()));
        }

        private void RemoveClassSpecificUser(String user)
        {
            var x = GetClassSpecificUser();
            x.Remove(user);
            AddOrUpdateClassSpecificUser(x);
        }

        private void RemoveClassSpecificUserGroups(string clientId)
        {
            var hg = GetClassSpecificGroup(this.ToString());
            hg.RemoveAll(r => r.Item1 == clientId);
        }
    }
}