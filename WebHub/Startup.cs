﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Owin;
using Microsoft.Owin;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;

[assembly: OwinStartup("ProductionConfiguration",typeof(WebHub.Startup))]
namespace WebHub
{
    public class Startup
    {
        public void Configuration(IAppBuilder app) {
            var hubConfigs = new HubConfiguration();
            hubConfigs.EnableDetailedErrors = true;
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR(hubConfigs);
        }

    }
}