﻿using Business;
using Global.Platform;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Routing;
using System.Linq;
using Microsoft.AspNet.SignalR;
using Global.DTO;
using System;

namespace WebHub.Controllers
{
    public class TheoController : ApiController
    {
        [Route("api/lookups/channels")]
        [HttpGet]
        public IEnumerable<string> GetChannels()
        {
            var x = new List<string> {
            "iCare","iWEB","IVR","PSS","KHANA"
            };
            return x;
        }

        [Route("api/lookups/clients")]
        [HttpGet]
        public IEnumerable<string> GetClients()
        {
            var x = new List<string> {
            "ATT","Sprint","Legacy"
            };
            return x;
        }

        [Route("api/lookups/environments")]
        [HttpGet]
        public IEnumerable<string> GetEnvironments()
        {
            var x = new List<string> {
            "QA","Prod","DEV","DEVA","DEVINT"
            };
            return x;
        }

        private IEnumerable<String> GetHubStaticGroups() {

            var x = TheoHub.UsersAndGroups.ContainsKey("WebHub.TheoHub");
            if (x)
            {
                var h = TheoHub.UsersAndGroups.Where(s => s.Key == "WebHub.TheoHub").Select(s => s.Value).FirstOrDefault();
                return h.Select(s => s.Item2).Distinct();
            }
            else { 
                return new List<String>();
            }
        }

        [Route("api/getgroups")]
        [HttpGet]
        public object GetGroups()
        {
            return GetHubStaticGroups();
        }

        [Route("api/getgroups/theo")]
        [HttpGet]
        public IEnumerable<string> GetGroupsTheo()
        {
            return GetHubStaticGroups();
        }

        [Route("api/lookups/layers")]
        [HttpGet]
        public IEnumerable<string> GetLayers()
        {
            var x = new List<string> {
            "UI","TIBCO","DB","BLACKBOX"
            };
            return x;
        }

        [Route("api/lookups/modules")]
        [HttpGet]
        public IEnumerable<string> GetModules()
        {
            var x = new List<string> {
            "PODA","PODB","PODC"
            };
            return x;
        }

        [Route("api/platformdefinition")]
        [HttpPost]
        public object GetPlatformDefinition(PlatformDefinition pk)
        {
            TheoBL tbl = new TheoBL();
            //PlatformKey pk = new PlatformKey("QA", "ATT", "iCare", "UI", "PODA");

            var platform = tbl.GetPlatformDefinition(pk);
            return platform._dict.Select(s => new { key = s.Key, val = s.Value });
        }


        [Route("api/getusers")]
        [HttpGet]
        public object GetUsers()
        {
            return TheoHub.HubUsers;
        }

        [Route("api/messagegroup")]
        [HttpPost]
        public void MessageGroup(dynamic d)
        {
            var th = GlobalHost.ConnectionManager.GetHubContext<TheoHub>();
            th.Clients.Group(d.groupname.Value).PingGroup(d.groupname.Value);
        }

        [Route("api/pinggroup")]
        [HttpPut]
        public bool PingGroup(CommandDTO commandDTO)
        {
            var th = GlobalHost.ConnectionManager.GetHubContext<TheoHub>();
            th.Clients.Group(commandDTO.CommandGroup).PingGroup(commandDTO);
            return true;
        }

        [Route("api/reloadplatformdefinition")]
        [HttpPost]
        public object ReloadPlatformDefinition(PlatformKey pk)
        {
            TheoBL tbl = new TheoBL();
            //PlatformKey pk = new PlatformKey("QA", "ATT", "iCare", "UI", "PODA");
            bool isTouched = false;
            var platformDefinition = tbl.ReloadPDF(pk, out isTouched);

            if (isTouched)
            {
                var th = GlobalHost.ConnectionManager.GetHubContext<TheoHub>();
                th.Clients.All.TheoTalkback("Your PDF has been changed.", platformDefinition);
            }

            return platformDefinition._dict.Select(s => new { key = s.Key, val = s.Value });
        }

        [Route("api/updatePdf")]
        [HttpPost]
        public void UpdatePDF(dynamic pk) {

            var x = new PlatformKey((string)pk.Environment,
                (string)pk.Client,
                (string)pk.Channel,
                (string)pk.Layer,
                (string)pk.Module);

            var k = (string)pk.key;
            var v = (string)pk.val;
            TheoBL tbl = new TheoBL();

            tbl.UpdatePDF(x, k, v);

        }
    }
}