﻿using Global.DTO;
using Global.Platform;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;

namespace EventRouter
{
    public class HubCommunicator
    {
        private IHubProxy _hub;
        public Listener listener;

        private static String[] ArgProcessor(string arg)
        {
            String[] args = new String[1];
            args[0] = arg;
            return args;
        }

        public HubCommunicator(string hubName, string hubUrl, string clientId)
        {
            if (String.IsNullOrEmpty(clientId))
            {
                clientId = Guid.NewGuid().ToString();
            }

            var connection = new HubConnection(hubUrl, "clientId=" + clientId);
            _hub = connection.CreateHubProxy(hubName);
            connection.Start().Wait();

            listener = new Listener(_hub);
        }

        public void TextToRoom(string room, string text)
        {
            _hub.Invoke("TextToRoom",room,text);
        }

        public string Hello()
        {
            var x = _hub.Invoke<String>("Hello");
            return x.Result;
        }

        public IHubProxy Hub
        {
            get
            {
                return _hub;
            }

            set { _hub = value; }
        }

        public List<String> GetAllGroups()
        {
            var x = _hub.Invoke<List<String>>("GetAllGroups");
            return x.Result;
        }

        public Dictionary<String, String> GetAllUsers()
        {
            var x = _hub.Invoke<Dictionary<String, String>>("GetAllUsers");
            return x.Result;
        }

        public void GroupEnrollment(string group)
        {
            var x = _hub.Invoke("JoinGroup", group);
            x.Wait();
        }

        public void SendOjbectTest(CommunicationDTO o)
        {
            _hub.Invoke<CommunicationDTO>("ObjectTest", o).Wait();
        }

        public PlatformDefinition GetPlatformDefinition(PlatformKey pk)
        {
            try
            {
                var x = _hub.Invoke<PlatformDefinition>("GetPlatformDefinition", pk);
                return x.Result;
            }
            catch (Exception ex)
            {
                return new PlatformDefinition();
            }
        }

        public Boolean CheckHash(PlatformKey pk, string hash)
        {
            try
            {
                var x = _hub.Invoke<Boolean>("CheckHash", pk, hash);
                return x.Result;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void ReloadPDF(PlatformKey pk)
        {
            _hub.Invoke("ReloadPDF", pk).Wait();
        }
    }
}