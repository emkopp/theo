﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;

namespace EventRouter
{
    public class Listener
    {
        private IHubProxy _hub;
        public static List<IDisposable> _listeners = new List<IDisposable>();

        public void RegisterListener<T>(String method, Action<T> action)
        {
            _listeners.Add(_hub.On<T>(method, action));
        }

        public void RegisterListener<T,K>(String method, Action<T,K> action)
        {
            _listeners.Add(_hub.On<T,K>(method, action));
        }


        public Listener(IHubProxy hub)
        {
            _hub = hub;
        }
    }
}