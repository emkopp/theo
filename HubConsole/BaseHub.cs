﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleHub
{
    public class BaseHub : Hub
    {
        public static Task _t;
        public static ConcurrentDictionary<String,List<String>> HubGroups = new ConcurrentDictionary<String,List<String>>();
        public static ConcurrentDictionary<String, Dictionary<String,String>> HubUsers = new ConcurrentDictionary<String, Dictionary<String,String>>();

        public BaseHub()
        {
        }

        public void ExitGroup(string groupName)
        {
            var message = String.Format("{2} : Group removal done for user {0} to Group {1}", GetClientId(), groupName, this.ToString());
            List<String> hg = GetClassSpecificGroup();
            hg.Remove(groupName);
            hg.Add(this.ToString());
            AddOrUpdateClassSpecificGroup(hg);
            Groups.Remove(Context.ConnectionId, groupName);
            Console.WriteLine(message);
            Clients.Caller.AdminFeed(message);
        }

        private void AddClassSpecificUser(String user, String key)
        {
            var x = GetClassSpecificUser();
            x.Add(user, key);
            AddOrUpdateClassSpecificUser(x);
        }

        private void RemoveClassSpecificUser(String user)
        {
            var x = GetClassSpecificUser();
            x.Remove(user);
            AddOrUpdateClassSpecificUser(x); 
        }

        private void AddOrUpdateClassSpecificUser(Dictionary<String,String> hsg)
        {
            HubUsers.AddOrUpdate(this.ToString(), hsg, (key, group) => group);
        }


        private void AddOrUpdateClassSpecificGroup(List<String> hsg) {
            HubGroups.AddOrUpdate(this.ToString(), hsg, (key, group) => group);
        }

        private List<String> GetClassSpecificGroup() {
            List<String> hg;
            if (!HubGroups.TryGetValue(this.ToString(), out hg)) {
                hg = new List<String>();
            }
            return hg;
        }

        private Dictionary<String,String> GetClassSpecificUser()
        {
            Dictionary<String,String> hg;
            if (!HubUsers.TryGetValue(this.ToString(), out hg))
            {
                hg = new Dictionary<String, String>();
            }
            return hg;
        }



        public IEnumerable<String> GetAllGroups()
        {
            return GetClassSpecificGroup();
        }

        public Dictionary<String, String> GetAllUsers()
        {
            return GetClassSpecificUser();
        }

        public void JoinGroup(string groupName)
        {
            List<String> hg = GetClassSpecificGroup();
            hg.Add(groupName);
            AddOrUpdateClassSpecificGroup(hg);
            Groups.Add(Context.ConnectionId, groupName);
            var message = String.Format("{2} : Group enrollment done for user {0} to Group {1}", GetClientId(), groupName, this.ToString());
            Console.WriteLine(message);
            Clients.Caller.AdminFeed(message);
        }

        public override Task OnConnected()
        {
            String clientId = GetClientId();
            if (!HubUsers.ContainsKey(clientId))
            {
                AddClassSpecificUser(clientId, Context.ConnectionId);
                Console.WriteLine(String.Format("{1} : User Connected: {0}",clientId, this.ToString()));
            }
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            string clientId = GetClientId();
            if (HubUsers.ContainsKey(clientId))
            {
                RemoveClassSpecificUser(clientId);
                Console.WriteLine(String.Format("{1} : User Disconnected: {0}", clientId, this.ToString()));
            }
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            String clientId = GetClientId();
            if (!HubUsers.ContainsKey(clientId))
            {
                AddClassSpecificUser(clientId, Context.ConnectionId);
                Console.WriteLine(String.Format("{1} : User Reconnected: {0}", clientId, this.ToString()));
            }
            return base.OnConnected();
        }
        protected string GetClientId()
        {
            String clientId = String.Empty;

            if (Context.QueryString["clientId"] != null)
            {
                clientId = this.Context.QueryString["clientId"];
            }
            if (String.IsNullOrEmpty(clientId))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }
    }
}