﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Owin;

namespace SignalRHub
{
    internal class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var hubConfigs = new HubConfiguration();
            hubConfigs.EnableDetailedErrors = true;

            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR(hubConfigs);
        }
    }
}