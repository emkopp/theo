﻿using Business;
using Global.Platform;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Concurrent;

namespace ConsoleHub
{
    [HubName("TheoHub")]
    public class TheoHub : BaseHub
    {
        private static ConcurrentDictionary<String, PlatformDefinition> _pdfList;
        private TheoBL _theoBl;

        public TheoHub()
        {
            _theoBl = new TheoBL();
        }

        public void RegisterApplication(String client, String channel, String layer, String environment, String module)
        {
            var pk = new PlatformKey(environment, client, channel, layer, module);
            RegisterApplication(pk);
        }

        public PlatformDefinition RegisterApplication(PlatformKey pk)
        {
            var pdf = _theoBl.GetPlatformDefinition(pk);
            return pdf;
        }

        public Boolean CheckHash(PlatformKey pk, String hash)
        {
            return _theoBl.IsHashSameAsStore(pk, hash);
        }

        public Boolean CheckTimestamp(PlatformKey pk, byte[] timestamp)
        {
            return _theoBl.IsTimestampSameAsStore(pk, timestamp);
        }

        public PlatformDefinition GetPlatformDefinition(PlatformKey pk)
        {
            var r = _theoBl.GetPlatformDefinition(pk);
            return r;
        }

        public void ReloadPDF(PlatformKey pk)
        {
            Boolean isTouched = false;
            var returnPDF = _theoBl.ReloadPDF(pk, out isTouched);
            if (isTouched)
            {
                Clients.All.TheoTalkback("Your PDF has been changed.", returnPDF);
            }
        }
    }
}