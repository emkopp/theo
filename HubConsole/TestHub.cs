﻿using Global.DTO;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleHub
{
    [HubName("TestHub")]
    public class TestHub : BaseHub
    {
        public static Task _t;

        public Task TimerCheck()
        {
            Action action = () =>
            {
                while (true)
                {
                    Clients.All.TimerCheck("Do Timer Check" + Context.ConnectionId);
                    Console.WriteLine("Checking Clients" + Context.ConnectionId);
                    Thread.Sleep(20000);
                }
            };
            return Task.Factory.StartNew(action);
        }

        public TestHub()
        {
            if (_t == null)
            {
                _t = TimerCheck();
            }
        }

        public void ObjectTest(CommunicationDTO message)
        {
            Console.WriteLine(message.Name);
            File.WriteAllBytes("test.jpg", message.Pic);
            var s = String.Format("Message received |{1}| and key |{2}| a image file of size {0} has been written to file system", message.Pic.Length.ToString(), message.Name, message.SomeNumericValue.ToString());
            Clients.All.ReceiveCom(s);

            var e = new CallBackDTO();
            e.Event = "Picture Taken";
            e.PicID = message.SomeNumericValue;
            Clients.All.ReceiveEvent(e);
            Clients.All.FormEvents("Object Test Fired: " + Context.ConnectionId);
        }
    }
}