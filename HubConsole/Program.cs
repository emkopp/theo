﻿using ConsoleHub;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Hosting;
using System;
using Global.Platform;

namespace SignalRHub
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string url = @"http://localhost:8080/";
            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine(string.Format("Server running at {0}", url));

                var theoHub = GlobalHost.ConnectionManager.GetHubContext<TheoHub>();
                var pk = new PlatformKey("QA", "ATT","iCare", "UI", "PODA");

                string cont = String.Empty;
                Console.WriteLine("type --> stop <-- to end loop.");
                while (cont != "stop")
                {



                    cont = Console.ReadLine();
                }

                Console.WriteLine("Press any key to end application.");
                
                
                
                
                
                
                Console.ReadLine();
            }
        }
    }
}