﻿using EventRouter;
using Global.Platform;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Global.DTO;
using Global.Extensions;
using Global.Platform;

namespace DemoWebApp
{
    public class Global : System.Web.HttpApplication
    {
        public static HubCommunicator _thc;
        public static PlatformDefinition _pdf;
        protected void Application_Start(object sender, EventArgs e)
        {
            var url = ConfigurationManager.AppSettings.Get("hub:url");
            _thc = new HubCommunicator("TheoHub", url, null);
            var thc2 = new HubCommunicator("TheoHub", url, null);
            PlatformKey pk = new PlatformKey("QA", "ATT", "iCare", "UI", "PODB");
            _pdf = _thc.GetPlatformDefinition(pk);

            _thc.GroupEnrollment("OTTO-CONNECTOR");
            _thc.GroupEnrollment(pk.GetKey());

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}